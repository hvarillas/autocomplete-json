FROM python:3.7-alpine

RUN apk add --no-cache tzdata
ENV TZ=America/Lima
RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \

RUN mkdir /app
ADD . /app
WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt
RUN apk del build-deps
EXPOSE 8066
CMD ["gunicorn", "-w4", "-b 0.0.0.0:8066", "app:api"]