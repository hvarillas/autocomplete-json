import falcon
import json
# from waitress import serve
from falcon.http_status import HTTPStatus
import urllib3

url = 'https://prix.tips/static/data/products.json'
http = urllib3.PoolManager()
data = http.request('GET', url).data.decode('utf-8')
js = json.loads(data)


class HandleCORS(object):
    def process_request(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header("content-type", "application/json")
        resp.set_header('Access-Control-Allow-Methods', '*')
        resp.set_header('Access-Control-Allow-Headers', '*')
        resp.set_header('Access-Control-Max-Age', 1728000)  # 20 days
        if req.method == 'OPTIONS':
            raise HTTPStatus(falcon.HTTP_200, body='\n')


def check_word(lst_word, word):
    w = False
    lst_tru = []
    for x in word:
        # print('{} / {}'.format(x, lst_word))
        if x in lst_word:
            lst_tru.append(True)
        else:
            lst_tru.append(False)
    if False in lst_tru:
        return False
    else:
        return True


class autocomplete:
    def on_post(self, req, res):
        word = json.loads(req.stream.read())
        word2 = word['word'].upper().split(' ')

        lst_words = []
        dic_words = {}
        for x in js:

            if check_word(x['title'], word2):
                lst_words.append(x['title'])
        dic_words['words'] = lst_words
        res.body = json.dumps(dic_words)


api = falcon.API(middleware=[HandleCORS()])
api.add_route('/', autocomplete())
# serve(api, listen="localhost:8000")
